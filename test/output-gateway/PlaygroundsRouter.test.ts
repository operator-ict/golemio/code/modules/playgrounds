import { playgroundsRouter } from "#og/PlaygroundsRouter";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("Playgrounds Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/playgrounds", playgroundsRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /playgrounds", (done) => {
        request(app).get("/playgrounds").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should respond with json to GET /playgrounds/:id", (done) => {
        request(app).get("/playgrounds/42").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should respond with json to GET /playgrounds/properties", (done) => {
        request(app)
            .get("/playgrounds/properties")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond to GET /playgrounds with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/playgrounds")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /playgrounds/:id with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/playgrounds/42")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /playgrounds/properties with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/playgrounds/properties")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });
});

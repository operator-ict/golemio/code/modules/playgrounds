import sinon, { SinonSpy, SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeocodeApi } from "@golemio/core/dist/integration-engine/helpers";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";
import { Playground } from "#sch/sequelize-models/Playground";
import { UpdateAddressAndDistrictTask } from "#ie/workers/playgrounds/tasks";

describe("UpdateAddressAndDistrictTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateAddressAndDistrictTask;
    let data0: IPlayground;
    let data1: Record<string, any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );

        data0 = { internal_id: 1, external_id: 1, location: { coordinates: [0, 0], type: "Point" } } as IPlayground;
        data1 = {
            internal_id: 2,
            external_id: 2,
            location: { coordinates: [1, 1], type: "Point" },
            save: sandbox.stub().resolves(true),
        };

        sandbox.stub(PlaygroundsInfo, "associateTables");

        task = new UpdateAddressAndDistrictTask("test.test");

        sandbox.stub(task["model"], "findOne").callsFake(() => Promise.resolve(data1));
        sandbox.stub(task["model"], "getPlayground").callsFake(() => Promise.resolve(data1 as Playground));
        sandbox.stub(task["model"], "getPlaygroundId").callsFake(() => Promise.resolve(1));

        sandbox.stub(task["cityDistrictsModel"], "findOne");
        sandbox.stub(GeocodeApi, "getAddressByLatLng");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by updateAddressAndDistrict method (different geo)", async () => {
        await task["execute"]({ external_id: data0.external_id });
        sandbox.assert.calledOnce(task["model"].findOne as SinonSpy);
        sandbox.assert.calledWith(task["model"].findOne as SinonSpy, { where: { external_id: data0.external_id } });

        sandbox.assert.calledOnce(task["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(GeocodeApi.getAddressByLatLng as SinonSpy);
        sandbox.assert.calledTwice(data1.save);
    });

    it("should calls the correct methods by updateAddressAndDistrict method (same geo)", async () => {
        let msgContent = {
            external_id: 1,
        } as IPlayground;
        data1.address_formatted = "a";
        data1.address_country = "CZ";
        data1.district = "praha-0";

        await task["execute"](msgContent);
        sandbox.assert.calledOnce(task["model"].findOne as SinonSpy);
        sandbox.assert.calledWith(task["model"].findOne as SinonSpy, { where: { external_id: data0.external_id } });

        sandbox.assert.calledOnce(task["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(GeocodeApi.getAddressByLatLng as SinonSpy);
        sandbox.assert.calledTwice(data1.save);
    });
});

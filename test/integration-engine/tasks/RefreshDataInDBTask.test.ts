import sinon, { SinonSpy, SinonSandbox } from "sinon";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";
import { Playground } from "#sch/sequelize-models/Playground";
import { RefreshDataInDBTask } from "#ie/workers/playgrounds/tasks";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    let testData: number[];
    let testTransformedData: IPlayground[];
    let data1: Record<string, any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );

        testData = [1, 2];
        testTransformedData = [
            { internal_id: 1, external_id: 1, location: { coordinates: [0, 0], type: "Point" } } as IPlayground,
            { internal_id: 2, external_id: 2, location: { coordinates: [1, 1], type: "Point" } } as IPlayground,
        ];

        data1 = {
            internal_id: 2,
            external_id: 2,
            location: { coordinates: [1, 1], type: "Point" },
            save: sandbox.stub().resolves(true),
        };

        sandbox.stub(PlaygroundsInfo, "associateTables");

        task = new RefreshDataInDBTask("test.test");

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(RefreshDataInDBTask.prototype, <any>"validateAndSavePlaygrounds");
        sandbox.stub(RefreshDataInDBTask.prototype, <any>"validateAndSaveProperties");

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
        sandbox.stub(task["model"], "findOne").callsFake(() => Promise.resolve(data1));
        sandbox.stub(task["model"], "getPlayground").callsFake(() => Promise.resolve(data1 as Playground));
        sandbox.stub(task["model"], "getPlaygroundId").callsFake(() => Promise.resolve(1));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledTwice(QueueManager["sendMessageToExchange"] as SinonSpy);

        for (const data of testTransformedData) {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.test",
                "updateAddressAndDistrict",
                {
                    external_id: data.external_id,
                }
            );
        }

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});

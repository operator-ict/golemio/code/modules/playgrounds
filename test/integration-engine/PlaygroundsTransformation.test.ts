import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { PlaygroundsTransformation } from "#ie/PlaygroundsTransformation";
import { IProperty } from "#sch/sequelize-models/interfaces/IProperty";
import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("PlaygroundsTransformation", () => {
    let transformation: PlaygroundsTransformation;
    let testSourceData: any[];

    beforeEach(async () => {
        transformation = new PlaygroundsTransformation();
        const buffer = await readFile(__dirname + "/data/playgrounds-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("Playgrounds");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData[0]);
        expect(data).to.have.property("location");
        expect(data).to.have.property("properties");
        if (data.properties.length > 0) {
            expect(data.properties[0]).to.have.property("id");
            expect(data.properties[0]).to.have.property("description");
        }
    });

    it("should properly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("location");
            expect(data[i]).to.have.property("properties");
            if (data[i].properties.length > 0) {
                expect(data[i].properties[0]).to.have.property("id");
                expect(data[i].properties[0]).to.have.property("description");
            }
        }
    });

    it("should select unique rows from array", async () => {
        const testData = [
            {
                properties: [
                    {
                        id: 1,
                        description: "test",
                    },
                    {
                        id: 1,
                        description: "test",
                    },
                    {
                        id: 2,
                        description: "test2",
                    },
                ] as IProperty[],
            },
            {
                properties: [
                    {
                        id: 3,
                        description: "test3",
                    },
                ] as IProperty[],
            },
        ] as IPlayground[];
        const expectedResult = [
            {
                id: 1,
                description: "test",
            },
            {
                id: 2,
                description: "test2",
            },
            {
                id: 3,
                description: "test3",
            },
        ] as IProperty[];
        const test = PlaygroundsTransformation.getUniqueProperties(testData);
        expect(test.length).to.be.eq(expectedResult.length);
        for (let index = 0; index < test.length; index++) {
            expect(test[index].id).to.be.eq(expectedResult[index].id);
            expect(test[index].description).to.be.eq(expectedResult[index].description);
        }
    });
});

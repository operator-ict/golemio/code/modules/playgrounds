import { AddressHelper } from "#ie/helpers/AddressHelper";
import { Playground } from "#sch/sequelize-models/Playground";
import { GeocodeApi } from "@golemio/core/dist/integration-engine";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("AddressHelper", () => {
    let sandbox: SinonSandbox;
    let playgroundTest: Playground;
    const testData = {
        address_formatted: "Komárovská 416/22, 19300 Hlavní město Praha-Horní Počernice, Česko",
        street_address: "Komárovská 416/22",
        postal_code: "19300",
        address_locality: "Hlavní město Praha",
        address_region: "Horní Počernice",
        address_country: "Česko",
    };

    before(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(GeocodeApi, "getAddressByLatLng" as any).callsFake(() => {
            return Promise.resolve(testData);
        });

        playgroundTest = {} as Playground;
        playgroundTest.internal_id = 1;
        playgroundTest.location = { coordinates: [0, 0], type: "Point" };
        playgroundTest.save = sandbox.stub();
    });

    after(() => {
        sandbox.restore();
    });

    it("updateAddress should update playground instance", async () => {
        await AddressHelper.updateAddress(playgroundTest);
        expect(playgroundTest.address_formatted).to.be.eq(testData.address_formatted);
        expect(playgroundTest.street_address).to.be.eq(testData.street_address);
        expect(playgroundTest.postal_code).to.be.eq(testData.postal_code);
        expect(playgroundTest.address_locality).to.be.eq(testData.address_locality);
        expect(playgroundTest.address_region).to.be.eq(testData.address_region);
        expect(playgroundTest.address_country).to.be.eq(testData.address_country);
    });
});

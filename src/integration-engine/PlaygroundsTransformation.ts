import { ISourcePlayground } from "#sch/json-schema/interfaces/ISourcePlayground";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";
import { IProperty } from "#sch/sequelize-models/interfaces/IProperty";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class PlaygroundsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = PlaygroundsInfo.moduleName;
    }

    protected transformElement = async (element: ISourcePlayground): Promise<IPlayground> => {
        const playground = {
            location: element.geometry,
            image_url: element.image && element.image.originalUrl ? element.image.originalUrl : null,
            content: element.content,
            external_id: element.itemId,
            name: this.removeIdentifier(element.title),
            perex: element.perex ? element.perex : "",
            url: element.url,
        } as IPlayground;

        playground.properties = element.category.map<IProperty>((element) => {
            return {
                id: element.id,
                description: element.nazev,
            };
        });

        return playground;
    };

    private removeIdentifier(title: string): string {
        return title.replace(/\d{1,2}\.(\D){0,1} /g, "");
    }

    public static getUniqueProperties = (playgroundsTransformedData: IPlayground[]): IProperty[] => {
        const result: IProperty[] = [];
        for (let index = 0; index < playgroundsTransformedData.length; index++) {
            const element = playgroundsTransformedData[index];
            if (element.properties) {
                for (let index = 0; index < element.properties.length; index++) {
                    const property = element.properties[index];
                    if (result.every((element) => element.id != property.id)) {
                        result.push(property);
                    }
                }
            }
        }

        return result;
    };
}

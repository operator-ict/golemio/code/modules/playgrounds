import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { RefreshDataInDBTask, UpdateAddressAndDistrictTask } from "./tasks";

export class PlaygroundsWorker extends AbstractWorker {
    protected name = PlaygroundsInfo.moduleName;

    constructor() {
        super();

        // Register tasks
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateAddressAndDistrictTask(this.getQueuePrefix()));
    }
}

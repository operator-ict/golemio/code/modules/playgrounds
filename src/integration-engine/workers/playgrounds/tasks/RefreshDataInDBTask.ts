import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";
import { IPlaygroundProperty } from "#sch/sequelize-models/interfaces/IPlaygroundProperty";
import { PlaygroundsModel } from "#ie/models/PlaygroundsModel";
import { AddressHelper } from "#ie/helpers/AddressHelper";
import { PlaygroundsTransformation } from "#ie/PlaygroundsTransformation";
import { PlaygroundsDataSource } from "#ie/PlaygroundsDataSource";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

    private dataSource: DataSource;
    private transformation: PlaygroundsTransformation;
    private model: PlaygroundsModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = PlaygroundsDataSource.get();
        this.transformation = new PlaygroundsTransformation();
        this.model = new PlaygroundsModel();
    }

    protected async execute() {
        try {
            const playgroundIdsForLocationSync = await this.savePlaygroundsData();

            // send messages for updating district and address and average occupancy
            const promises = playgroundIdsForLocationSync.map((external_id: number) => {
                QueueManager.sendMessageToExchange(this.queuePrefix, "updateAddressAndDistrict", { external_id });
            });

            await Promise.all(promises);
        } catch (exception) {
            throw exception instanceof AbstractGolemioError
                ? exception
                : new GeneralError("Error while refreshing playgrounds data", this.constructor.name, exception);
        }
    }

    private async savePlaygroundsData(): Promise<number[]> {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const result = await this.getTransformedDataWithId();

            await this.validateAndSavePlaygrounds(result.transformedData, t);
            await this.validateAndSaveProperties(result.transformedData, t);

            await t.commit();

            return result.playgroundIdsForLocationSync;
        } catch (err) {
            await t.rollback();
            throw err;
        }
    }

    private async validateAndSavePlaygrounds(transformedData: IPlayground[], t: Transaction) {
        this.model.validate(transformedData);
        const promises = transformedData.map(async (playground) => {
            await this.model["sequelizeModel"].upsert({ ...playground }, { transaction: t });
        });
        await Promise.all(promises);
    }

    private async validateAndSaveProperties(transformedData: IPlayground[], t: Transaction) {
        const propertiesData = PlaygroundsTransformation.getUniqueProperties(transformedData);
        const propertiesRelationData = await this.getPlaygroundPropertyRelationData(transformedData, t);

        this.model.propertiesModel.validate(propertiesData);
        this.model.playgroundPropertyModel.validate(propertiesRelationData);
        const promises = propertiesData.map(async (property) => {
            await this.model.propertiesModel["sequelizeModel"].upsert({ ...property }, { transaction: t });
        });
        await Promise.all(promises);
        const promises2 = propertiesRelationData.map(async (playgroundProperty) => {
            await this.model.playgroundPropertyModel["sequelizeModel"].upsert({ ...playgroundProperty }, { transaction: t });
        });
        await Promise.all(promises2);
    }

    private getTransformedDataWithId = async (): Promise<{
        transformedData: IPlayground[];
        playgroundIdsForLocationSync: number[];
    }> => {
        const data = await this.dataSource.getAll();
        const transformedData: IPlayground[] = await this.transformation.transform(data);
        const dataIdsWithNewLocation: number[] = [];

        for (let index = 0; index < transformedData.length; index++) {
            const playground = transformedData[index];
            const dbPlayground = await this.model.getPlayground(playground.external_id);
            // get internal ids for already existing data
            if (dbPlayground) {
                playground.internal_id = dbPlayground.internal_id;
                if (AddressHelper.needsUpdate(playground, dbPlayground)) {
                    dataIdsWithNewLocation.push(playground.external_id);
                }
            } else {
                // new playground
                dataIdsWithNewLocation.push(playground.external_id);
            }
        }

        return { transformedData: transformedData, playgroundIdsForLocationSync: dataIdsWithNewLocation };
    };

    private getPlaygroundPropertyRelationData = async (
        playgroundsTransformedData: IPlayground[],
        t: Transaction
    ): Promise<IPlaygroundProperty[]> => {
        const result: IPlaygroundProperty[] = [];
        for (let index = 0; index < playgroundsTransformedData.length; index++) {
            const element = playgroundsTransformedData[index];
            const internalId = element.internal_id ?? (await this.model.getPlaygroundId(element.external_id, t));

            if (element.properties) {
                for (let index = 0; index < element.properties.length; index++) {
                    const property = element.properties[index];
                    if (internalId) {
                        result.push({
                            playground_id: internalId,
                            properties_id: property.id,
                        });
                    }
                }
            }
        }

        return result;
    };
}

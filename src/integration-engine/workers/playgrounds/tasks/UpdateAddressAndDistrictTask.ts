import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { Playground } from "#sch/sequelize-models/Playground";
import { AddressHelper } from "#ie/helpers/AddressHelper";
import { IPlaygroundInput, PlaygroundsValidationSchema } from "#ie/schema";
import { PlaygroundsModel } from "#ie/models/PlaygroundsModel";

export class UpdateAddressAndDistrictTask extends AbstractTask<IPlaygroundInput> {
    public readonly queueName = "updateAddressAndDistrict";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = PlaygroundsValidationSchema;

    private readonly model: PlaygroundsModel;
    private readonly cityDistrictsModel: CityDistrictsModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.model = new PlaygroundsModel();
        this.cityDistrictsModel = new CityDistrictsModel();
    }

    protected async execute(data: IPlaygroundInput) {
        const dbData: Playground = await this.model.findOne({
            where: {
                external_id: data.external_id,
            },
        });

        await AddressHelper.updateDistrict(dbData, this.cityDistrictsModel);
        await AddressHelper.updateAddress(dbData);
    }
}

import { IsNumber } from "@golemio/core/dist/shared/class-validator";

export interface IPlaygroundInput {
    external_id: number;
}

export class PlaygroundsValidationSchema implements IPlaygroundInput {
    @IsNumber()
    external_id!: number;
}

import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";
import { Playground } from "#sch/sequelize-models/Playground";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { GeocodeApi } from "@golemio/core/dist/integration-engine";
import { RecoverableError } from "@golemio/core/dist/shared/golemio-errors";
import { Point } from "geojson";

export class AddressHelper {
    public static isLocationChanged = (point1: Point, point2: Point): boolean => {
        return point1.coordinates[0] !== point2.coordinates[0] || point1.coordinates[1] !== point2.coordinates[1];
    };

    public static isMissing = (playground: Playground): boolean => {
        return !playground.address_formatted || !playground.address_country || !playground.district;
    };

    public static needsUpdate = (newPlayground: IPlayground, currentDbPlayground: Playground): boolean => {
        return (
            AddressHelper.isLocationChanged(newPlayground.location, currentDbPlayground.location) ||
            AddressHelper.isMissing(currentDbPlayground)
        );
    };

    public static async updateAddress(dbData: Playground) {
        try {
            const address = await GeocodeApi.getAddressByLatLng(dbData.location.coordinates[1], dbData.location.coordinates[0]);
            if (address) {
                dbData.address_country = address.address_country ?? "";
                dbData.address_formatted = address.address_formatted ?? "";
                dbData.address_locality = address.address_locality ?? "";
                dbData.address_region = address.address_region ?? "";
                dbData.street_address = address.street_address ?? "";
                dbData.postal_code = address.postal_code ?? "";
            }

            await dbData.save();
        } catch (err) {
            throw new RecoverableError("Error while updating address.", this.constructor.name, err);
        }
    }

    public static async updateDistrict(dbData: Playground, cityDistrictsModel: CityDistrictsModel) {
        try {
            dbData.district = await cityDistrictsModel.getDistrict(
                dbData.location.coordinates[0],
                dbData.location.coordinates[1]
            );
            await dbData.save();
        } catch (err) {
            throw new RecoverableError("Error while updating district.", this.constructor.name, err);
        }
    }
}

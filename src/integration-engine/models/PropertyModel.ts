import { OutputSchemaProvider } from "#sch/json-schema/OutputSchemaProvider";
import { PlaygroundSchemaTables } from "#sch/PlaygroundSchemaTables";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { Property } from "#sch/sequelize-models/Property";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class PropertyModel extends PostgresModel implements IModel {
    constructor() {
        super(
            PlaygroundsInfo.propertiesModelName,
            {
                outputSequelizeAttributes: Property.attributeModel,
                pgTableName: PlaygroundSchemaTables.Properties,
                pgSchema: PlaygroundsInfo.schemaName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("PropertyModelValidator", OutputSchemaProvider.property, true)
        );
    }
}

import { OutputSchemaProvider } from "#sch/json-schema/OutputSchemaProvider";
import { PlaygroundSchemaTables } from "#sch/PlaygroundSchemaTables";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { PlaygroundProperty } from "#sch/sequelize-models/PlaygroundProperty";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class PlaygroundPropertyModel extends PostgresModel implements IModel {
    constructor() {
        super(
            PlaygroundsInfo.playgroundPropertiesModelName,
            {
                outputSequelizeAttributes: PlaygroundProperty.attributeModel,
                pgTableName: PlaygroundSchemaTables.PlaygroundProperties,
                pgSchema: PlaygroundsInfo.schemaName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("PlaygroundsPropertyModelValidator", OutputSchemaProvider.playgroundProperty, true)
        );
    }
}

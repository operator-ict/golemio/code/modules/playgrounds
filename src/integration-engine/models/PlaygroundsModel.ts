import { OutputSchemaProvider } from "#sch/json-schema/OutputSchemaProvider";
import { PlaygroundSchemaTables } from "#sch/PlaygroundSchemaTables";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { Playground } from "#sch/sequelize-models/Playground";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import sequelize from "@golemio/core/dist/shared/sequelize";
import { PlaygroundPropertyModel } from "./PlaygroundPropertyModel";
import { PropertyModel } from "./PropertyModel";

export class PlaygroundsModel extends PostgresModel implements IModel {
    public propertiesModel: PropertyModel;
    public playgroundPropertyModel: PlaygroundPropertyModel;

    constructor() {
        super(
            PlaygroundsInfo.playgroundsModelName,
            {
                outputSequelizeAttributes: Playground.attributesModel,
                pgTableName: PlaygroundSchemaTables.Playground,
                pgSchema: PlaygroundsInfo.schemaName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("PlaygroundsModelValidator", OutputSchemaProvider.playgroundOutput, true)
        );
        this.propertiesModel = new PropertyModel();
        this.playgroundPropertyModel = new PlaygroundPropertyModel();

        PlaygroundsInfo.associateTables(
            this.sequelizeModel,
            this.propertiesModel["sequelizeModel"],
            this.playgroundPropertyModel["sequelizeModel"]
        );
    }

    public getPlaygroundId = async (external_id: number, t?: sequelize.Transaction): Promise<number | undefined> => {
        const playground = await this.getPlayground(external_id, t);

        return playground ? playground.internal_id : undefined;
    };

    public async getPlayground(external_id: number, t?: sequelize.Transaction | undefined): Promise<Playground | null> {
        return await this.sequelizeModel.findOne<Playground>({
            where: { external_id: external_id },
            transaction: t,
        });
    }
}

import { SourceSchemaProvider } from "#sch/json-schema/SourceSchemaProvider";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class PlaygroundsDataSource {
    private static dataSourceName = "PlaygroundsDataSource";

    public static get = (): DataSource => {
        return new DataSource(
            this.dataSourceName,
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.Playgrounds,
            }),
            new JSONDataTypeStrategy({ resultsPath: "items" }),
            new JSONSchemaValidator(this.dataSourceName + "Validator", SourceSchemaProvider.playground, true)
        );
    };
}

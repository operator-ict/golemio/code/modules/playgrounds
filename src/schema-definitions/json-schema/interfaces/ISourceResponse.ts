import { ISourcePlayground } from "./ISourcePlayground";

export interface ISourceResponse {
    items: ISourcePlayground[];
    request: { type: string };
    totalNumberOfItems: Number;
}

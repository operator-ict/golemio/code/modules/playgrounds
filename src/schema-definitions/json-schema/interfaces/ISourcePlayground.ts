import { Geometry } from "geojson";
import { ISourcePlaygroundCategory } from "./ISourcePlaygroundCategory";

export interface ISourcePlayground {
    itemId: number;
    url: string;
    title: string;
    geometry: Geometry;
    perex: string;
    content: string;
    category: ISourcePlaygroundCategory[];
    image: { originalUrl: string };
}

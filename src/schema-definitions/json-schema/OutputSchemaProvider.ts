import { IPlayground } from "#sch/sequelize-models/interfaces/IPlayground";
import { IPlaygroundProperty } from "#sch/sequelize-models/interfaces/IPlaygroundProperty";
import { IProperty } from "#sch/sequelize-models/interfaces/IProperty";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class OutputSchemaProvider {
    public static playgroundOutput: JSONSchemaType<IPlayground[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                internal_id: { type: "integer" },
                location: { $ref: "#/definitions/geometry" },
                image_url: { type: "string" },
                content: { type: "string" },
                external_id: { type: "integer" },
                name: { type: "string" },
                perex: { type: "string" },
                url: { type: "string" },
                district: { type: "string" },
                address_country: { type: "string" },
                address_formatted: { type: "string" },
                address_locality: { type: "string" },
                address_region: { type: "string" },
                postal_code: { type: "string" },
                street_address: { type: "string" },
            },
            required: ["location", "content", "external_id", "name", "url"],
        },
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };

    public static property: JSONSchemaType<IProperty[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                description: { type: "string" },
            },
            required: ["id", "description"],
        },
    };

    public static playgroundProperty: JSONSchemaType<IPlaygroundProperty[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                playground_id: { type: "integer" },
                properties_id: { type: "integer" },
            },
            required: ["playground_id", "properties_id"],
        },
    };
}

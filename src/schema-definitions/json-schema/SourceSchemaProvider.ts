import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ISourcePlayground } from "./interfaces/ISourcePlayground";

export class SourceSchemaProvider {
    public static playground: JSONSchemaType<ISourcePlayground[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                itemId: { type: "integer" },
                url: { type: "string" },
                title: { type: "string" },
                geometry: { $ref: "#/definitions/geometry" },
                perex: { type: "string" },
                content: { type: "string" },
                category: { $ref: "#/definitions/category" },
                image: { $ref: "#/definitions/image" },
            },
            required: ["itemId", "url", "title", "geometry", "content", "image"], //TODO is image required?
            additionalProperties: false,
        },
        definitions: {
            category: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        nazev: { type: "string" },
                        id: { type: "integer" },
                    },
                    required: ["id", "nazev"],
                },
            },
            image: {
                type: "object",
                properties: {
                    originalUrl: { type: "string" },
                },
                required: ["originalUrl"],
            },
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}

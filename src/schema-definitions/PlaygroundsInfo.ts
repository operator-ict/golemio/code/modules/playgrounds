import Sequelize from "@golemio/core/dist/shared/sequelize";
import { Playground } from "./sequelize-models/Playground";
import { PlaygroundProperty } from "./sequelize-models/PlaygroundProperty";
import { Property } from "./sequelize-models/Property";
export class PlaygroundsInfo {
    public static schemaName = "playgrounds";
    public static moduleName = "Playgrounds";
    public static playgroundsModelName = "PlaygroundsModel";
    public static propertiesModelName = "PropertyModel";
    public static playgroundPropertiesModelName = "PlaygroundCategoryModel";

    public static associateTables(
        mainTable: Sequelize.ModelCtor<Playground>,
        secondaryTable: Sequelize.ModelCtor<Property>,
        relationTable: Sequelize.ModelCtor<PlaygroundProperty>
    ) {
        mainTable.belongsToMany(secondaryTable, {
            through: relationTable,
            foreignKey: "playground_id",
            otherKey: "properties_id",
            as: "properties",
        });
        secondaryTable.belongsToMany(mainTable, {
            through: relationTable,
            foreignKey: "properties_id",
            otherKey: "playground_id",
        });
        relationTable.belongsTo(mainTable, {
            targetKey: "internal_id",
            foreignKey: "playground_id",
        });
        relationTable.belongsTo(secondaryTable, {
            targetKey: "id",
            foreignKey: "properties_id",
        });
        mainTable.hasMany(relationTable, {
            sourceKey: "internal_id",
            foreignKey: "playground_id",
        });
        secondaryTable.hasMany(relationTable, {
            sourceKey: "id",
            foreignKey: "properties_id",
        });
    }
}

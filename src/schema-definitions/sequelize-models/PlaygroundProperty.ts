import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IPlaygroundProperty } from "./interfaces/IPlaygroundProperty";

export class PlaygroundProperty extends Model<PlaygroundProperty> implements IPlaygroundProperty {
    declare playground_id: number;
    declare properties_id: number;

    public static attributeModel: ModelAttributes<PlaygroundProperty> = {
        playground_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            references: {
                model: PlaygroundsInfo.playgroundsModelName,
                key: "internal_id",
            },
        },
        properties_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            references: {
                model: PlaygroundsInfo.propertiesModelName,
                key: "id",
            },
        },
    };
}

export interface IProperty {
    id: number;
    description: string;
}

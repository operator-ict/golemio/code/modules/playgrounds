import { Point } from "geojson";
import { IProperty } from "./IProperty";

export interface IPlayground {
    internal_id: number | undefined;
    location: Point;
    image_url: string | null;
    content: string;
    external_id: number;
    name: string;
    perex: string;
    url: string;
    district: string;
    address_country: string;
    address_formatted: string;
    address_locality: string;
    address_region: string;
    postal_code: string;
    street_address: string;
    properties: IProperty[];
}

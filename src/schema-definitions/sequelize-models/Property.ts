import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IProperty } from "./interfaces/IProperty";

export class Property extends Model<Property> implements IProperty {
    declare id: number;
    declare description: string;

    public static attributeModel: ModelAttributes<Property> = {
        id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true },
        description: { type: DataTypes.STRING },
    };
}

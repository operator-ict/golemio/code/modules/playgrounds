import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { Point } from "geojson";
import { IPlayground } from "./interfaces/IPlayground";
import { IProperty } from "./interfaces/IProperty";

export class Playground extends Model<Playground> implements IPlayground {
    declare internal_id: number;
    declare location: Point;
    declare image_url: string;
    declare content: string;
    declare external_id: number;
    declare name: string;
    declare perex: string;
    declare url: string;
    declare district: string;
    declare address_country: string;
    declare address_formatted: string;
    declare address_locality: string;
    declare address_region: string;
    declare postal_code: string;
    declare street_address: string;
    declare updated_at: string;
    public properties: IProperty[] = [];

    public static attributesModel: ModelAttributes<Playground> = {
        internal_id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true, autoIncrement: true },
        location: { type: DataTypes.GEOMETRY },
        image_url: { type: DataTypes.STRING, allowNull: true },
        content: { type: DataTypes.STRING },
        external_id: { type: DataTypes.STRING },
        name: { type: DataTypes.STRING },
        perex: { type: DataTypes.STRING, allowNull: true },
        url: { type: DataTypes.STRING },
        district: { type: DataTypes.STRING, allowNull: true },
        address_country: { type: DataTypes.STRING },
        address_formatted: { type: DataTypes.STRING },
        address_locality: { type: DataTypes.STRING },
        address_region: { type: DataTypes.STRING },
        postal_code: { type: DataTypes.STRING },
        street_address: { type: DataTypes.STRING },
        updated_at: { type: DataTypes.DATE },
    };
}

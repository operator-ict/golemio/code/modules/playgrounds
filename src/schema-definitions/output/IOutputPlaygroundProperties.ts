export interface IOutputPlaygroundProperties {
    image: { url: string };
    content: string;
    id: number;
    name: string;
    perex: string;
    properties: Array<{ description: string; id: number }>;
    updated_at: string;
    url: string;
    district: string;
    address: {
        address_country: string;
        address_formatted: string;
        street_address: string;
        postal_code: string;
        address_locality: string;
        address_region: string;
    };
}

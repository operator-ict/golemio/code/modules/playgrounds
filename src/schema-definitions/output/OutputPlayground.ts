import { Playground } from "#sch/sequelize-models/Playground";
import { GeoCoordinatesType, IGeoJSONFeature, TGeoCoordinates } from "@golemio/core/dist/output-gateway";
import { Point } from "geojson";
import { IOutputPlaygroundProperties } from "./IOutputPlaygroundProperties";

export class OutputPlayground implements IGeoJSONFeature {
    public geometry: TGeoCoordinates;
    public properties: IOutputPlaygroundProperties;
    public type: "Feature";

    constructor(playground: Playground) {
        this.geometry = {} as TGeoCoordinates;
        this.geometry.coordinates = (playground.location as Point).coordinates;
        this.geometry.type = GeoCoordinatesType.Point;
        this.properties = {
            image: { url: playground.image_url },
            content: playground.content,
            id: playground.external_id,
            name: playground.name,
            perex: playground.perex,
            properties: playground.properties.map((property) => {
                return {
                    description: property.description,
                    id: property.id,
                };
            }),
            updated_at: playground.updated_at,
            url: playground.url,
            district: playground.district,
            address: {
                address_country: playground.address_country,
                address_formatted: playground.address_formatted,
                street_address: playground.street_address,
                postal_code: playground.postal_code,
                address_locality: playground.address_locality,
                address_region: playground.address_region,
            },
        };
        this.type = "Feature";
    }
}

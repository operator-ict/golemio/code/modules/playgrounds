export enum PlaygroundSchemaTables {
    Playground = "playground",
    Properties = "properties",
    PlaygroundProperties = "playground_properties",
}

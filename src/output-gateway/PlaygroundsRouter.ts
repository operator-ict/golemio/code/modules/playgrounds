/**
 * PlaygroundsRouter.ts
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { checkErrors, GeoJsonRouter } from "@golemio/core/dist/output-gateway";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { PlaygroundsModel } from "./models/PlaygroundsModel";

const CACHE_MAX_AGE = 12 * 60 * 60;
const CACHE_STALE_WHILE_REVALIDATE = 60 * 60;

export class PlaygroundsRouter extends GeoJsonRouter {
    public router: Router = Router();

    constructor() {
        super(new PlaygroundsModel());
        this.initRoutes({ maxAge: CACHE_MAX_AGE, staleWhileRevalidate: CACHE_STALE_WHILE_REVALIDATE });
        this.router.get(
            "/properties",
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetProperties
        );
    }

    public GetProperties = (req: Request, res: Response, next: NextFunction) => {
        (this.model as PlaygroundsModel)
            .GetProperties()
            .then((data) => {
                res.status(200).send(data);
            })
            .catch((err) => {
                next(err);
            });
    };
}

const playgroundsRouter: Router = new PlaygroundsRouter().router;

export { playgroundsRouter };

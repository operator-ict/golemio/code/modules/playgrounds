import { PlaygroundSchemaTables } from "#sch/PlaygroundSchemaTables";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { Property } from "#sch/sequelize-models/Property";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class PropertyModel extends SequelizeModel {
    constructor() {
        super(PlaygroundsInfo.propertiesModelName, PlaygroundSchemaTables.Properties, Property.attributeModel, {
            schema: PlaygroundsInfo.schemaName,
        });
    }

    GetAll(options?: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}

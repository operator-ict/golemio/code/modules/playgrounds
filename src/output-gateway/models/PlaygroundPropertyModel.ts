import { PlaygroundSchemaTables } from "#sch/PlaygroundSchemaTables";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { PlaygroundProperty } from "#sch/sequelize-models/PlaygroundProperty";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class PlaygroundPropertyModel extends SequelizeModel {
    constructor() {
        super(
            PlaygroundsInfo.playgroundPropertiesModelName,
            PlaygroundSchemaTables.PlaygroundProperties,
            PlaygroundProperty.attributeModel,
            {
                schema: PlaygroundsInfo.schemaName,
            }
        );
    }

    GetAll(options?: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}

import { OutputPlayground } from "#sch/output/OutputPlayground";
import { PlaygroundSchemaTables } from "#sch/PlaygroundSchemaTables";
import { PlaygroundsInfo } from "#sch/PlaygroundsInfo";
import { IProperty } from "#sch/sequelize-models/interfaces/IProperty";
import { Playground } from "#sch/sequelize-models/Playground";
import { Property } from "#sch/sequelize-models/Property";
import {
    buildGeojsonFeatureCollection,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
    IPropertyResponseModel,
    SequelizeModel,
} from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { FilterHelper } from "./FilterHelper";
import { PlaygroundPropertyModel } from "./PlaygroundPropertyModel";
import { PropertyModel } from "./PropertyModel";

export class PlaygroundsModel extends SequelizeModel implements IGeoJsonModel {
    public propertiesModel: PropertyModel;
    public playgroundPropertyModel: PlaygroundPropertyModel;

    constructor() {
        super(PlaygroundsInfo.playgroundsModelName, PlaygroundSchemaTables.Playground, Playground.attributesModel, {
            schema: PlaygroundsInfo.schemaName,
        });
        this.propertiesModel = new PropertyModel();
        this.playgroundPropertyModel = new PlaygroundPropertyModel();

        PlaygroundsInfo.associateTables(
            this.sequelizeModel,
            this.propertiesModel.sequelizeModel,
            this.playgroundPropertyModel.sequelizeModel
        );
    }

    public IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(true);
    }

    public PrimaryIdentifierSelection(inId: string): object {
        return { internal_id: inId };
    }

    public GetAll = async (options?: IGeoJsonAllFilterParameters): Promise<IGeoJSONFeatureCollection> => {
        const result = await this.sequelizeModel.findAll<Playground>({
            attributes: {
                include: ["updated_at"],
            },
            include: "properties",
            where: options
                ? {
                      [Sequelize.Op.and]: [
                          ...FilterHelper.prepareFilterForLocation(options),
                          ...FilterHelper.prepareFilterForUpdateSince(options),
                          ...FilterHelper.prepareFilterForDistricts(options),
                      ],
                  }
                : {},
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order: FilterHelper.prepareOrderFunction(options),
        });

        return buildGeojsonFeatureCollection(result.map((element) => this.CreateOutputItem(element)));
    };

    public GetOne = async (id: any): Promise<IGeoJSONFeature | undefined> => {
        const result = await this.sequelizeModel.findOne<Playground>({
            attributes: {
                include: ["updated_at"],
            },
            where: { external_id: id },
            include: "properties",
        });

        return this.CreateOutputItem(result);
    };

    public GetProperties = async (): Promise<IPropertyResponseModel[]> => {
        const result: IProperty[] = await this.propertiesModel.sequelizeModel.findAll<Property>();

        return result.map((x: IProperty) => {
            const property: IPropertyResponseModel = {
                id: x.id,
                title: x.description,
            };
            return property;
        });
    };

    private CreateOutputItem = (result: Playground | null): IGeoJSONFeature | undefined => {
        if (!result) return undefined;

        return new OutputPlayground(result);
    };
}

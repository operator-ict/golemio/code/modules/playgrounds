# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.11.13] - 2024-11-05

### Fixed

-   fix update address task

## [1.1.12] - 2024-09-05

### Added

-   asyncAPI merge yaml files ([integration-engine#258](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/258))

## [1.1.11] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.10] - 2024-07-17

### Fixed

-   API validations ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [1.1.9] - 2024-06-03

### Added

-   The `Cache-Control` header to all output gateway responses ([core#105](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/105))

### Removed

-   MongoDB remains

### Fixed

-   Missing API version prefix in OpenAPI docs

## [1.1.8] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.7] - 2024-04-08

### Fixed

-   API docs inconsistencies (https://gitlab.com/operator-ict/golemio/code/modules/playgrounds/-/merge_requests/54)

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.1.6] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.1.5] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.4] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.3] - 2023-05-29

### Changed

-   Update openapi docs

## [1.1.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-22

### Changed

-   Update city-districts

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.7] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.6] - 2022-08-24

### Added

-   !apidoc to npm.ignore ([IG#79](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/79))
-   Create new separate worker and tasks with validations ([playgrounds#5](https://gitlab.com/operator-ict/golemio/code/modules/playgrounds/-/issues/5))

## [1.0.5] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.4] - 2021-03-31

### Fixed

-   sorting by distance on get all playgrounds,
-   exchange of lng and lat

## [1.0.3] - 2021-03-16

### Added

-   openapi: 3.0.3 doc

### Changed

-   migrated from Mongo DB to postgres


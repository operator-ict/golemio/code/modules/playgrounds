-- playgrounds.properties definition

CREATE TABLE properties (
	"id" int4 NOT NULL,
	"description" text NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
	CONSTRAINT properties_pk PRIMARY KEY ("id")
);

-- playgrounds.playground definition

CREATE TABLE playground (
	"internal_id" serial4 NOT NULL,
	"location" geometry NULL,
	"content" text NOT NULL,
	"external_id" int4 NOT NULL,
	"name" text NOT NULL,
	"perex" text NULL,
	"updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
	"url" text NOT NULL,
    "image_url" text NOT NULL,
	"district" text NULL,
    "address_region" varchar NULL,
    "address_country" varchar NULL,
	"address_formatted" text NULL,
	"address_locality" varchar NULL,
	"postal_code" varchar NULL,
	"street_address" varchar NULL,
    CONSTRAINT playground_pkey PRIMARY KEY ("internal_id"),
    CONSTRAINT playground_un UNIQUE ("external_id")
);
CREATE INDEX idx_playground_geom ON playground USING gist ("location");
CREATE INDEX playground_district_idx ON playground USING btree ("district");
CREATE UNIQUE INDEX playground_external_id_idx ON playground USING btree ("external_id");
CREATE INDEX playground_updated_at_idx ON playground USING btree ("updated_at");


-- playgrounds.playground_properties definition

CREATE TABLE playground_properties (
	"playground_id" int4 NOT NULL,
	"properties_id" int4 NOT NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
	CONSTRAINT playground_fk FOREIGN KEY ("playground_id") REFERENCES playground("internal_id") ON DELETE CASCADE,
	CONSTRAINT playground_properties_fk FOREIGN KEY ("playground_id") REFERENCES playground("internal_id") ON DELETE CASCADE,
	CONSTRAINT properties_fk FOREIGN KEY ("properties_id") REFERENCES properties("id") ON DELETE CASCADE,
    CONSTRAINT playground_properties_pk PRIMARY KEY ("playground_id", "properties_id")
);


TRUNCATE
    "playground_properties",
    "properties",
    "playground";

ALTER SEQUENCE playground_internal_id_seq RESTART WITH 1;

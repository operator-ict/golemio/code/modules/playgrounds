INSERT INTO properties (id,description,updated_at,created_at) VALUES
	 (4,'Fitness prvky','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (9,'Kultura (muzea, galerie, venkovní sochy, divadla, planetária)','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (5,'Voda-hydrant nebo umyvadlo','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (6,'WC na hřišti nebo v bezprostřední blízkosti','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (8,'Částečný stín','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (3,'Jiné sporty (lanová centra, minigolf, discgolf, boby)','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (13,'Bazény, vodní atrakce, vodní soustavy','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (1,'Plocha pro míčové hry','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (2,'In line stezky, skate parky, BMX areály, dopravní hřiště','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (7,'Správce na hřišti','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672');
INSERT INTO properties (id,description,updated_at,created_at) VALUES
	 (12,'Restaurace nebo kavárny v bezprostřední blízkosti','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (10,'Naučné stezky','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672'),
	 (11,'ZOO koutky','2022-03-09 10:17:31.703','2022-03-09 10:16:05.672');

INSERT INTO playgrounds.playground ("internal_id","location","content",external_id,"name",perex,updated_at,created_at,url,image_url,district,address_region,address_country,address_formatted,address_locality,postal_code,street_address) VALUES
	 (1,'SRID=4326;POINT (14.613753319 50.11164856)'::geometry,'Dětské hřiště (62.A) se rozkládá v parčíku, u konečné autobusu č. 223. Tvoří ho hned 3 části. První je oplocena vyšším...',89,' Horní Počernice - hřiště 62.A','Lokalita vám nabídne velmi pěkné hřiště a několik zajímavých doprovodných aktivit.','2022-03-09 10:17:31.063','2022-03-09 10:16:03.574','http://www.hristepraha.cz/hriste/mapa/horni-pocernice-hriste-62-a','http://www.hristepraha.cz/images/img/840ce044e65195caa5465818342e3ac4o.jpg','Praha 20','Horní Počernice','Česko','Běluňská, 19300 Hlavní město Praha-Horní Počernice, Česko','Hlavní město Praha','19300','Běluňská'),
	 (2,'SRID=4326;POINT (14.428703308 50.130802155)'::geometry,'Větší hřiště se rozkládá, mezi ulicemi Toruňská a Gdaňská (29.A). Děti mohou vyzkoušet asi deset atrakcí. Atraktivní je...',42,'Bohnice a Čimice - Bohnice','Nedaleko od sebe leží 2 pěkná hřiště.','2022-03-09 10:17:31.063','2022-03-09 10:16:03.575','http://www.hristepraha.cz/hriste/mapa/bohnice-a-cimice-bohnice','http://www.hristepraha.cz/images/img/124f60c9d87bda76b3a42d3b6730bdf6o.jpg','Praha 8','Bohnice','Česko','Toruňská 326/3, 18100 Hlavní město Praha-Bohnice, Česko','Hlavní město Praha','18100','Toruňská 326/3'),
	 (3,'SRID=4326;POINT (14.338401794 50.048431396)'::geometry,'U stanice metra Lužiny se rozkládá rozlehlý park. Přestože je místy poněkud zanedbaný, stojí za návštěvu. Procházku...',66,'Centrální park Prahy 13','Chcete si prohlédnout Central park? Pak není nutné trmácet se do New Yorku. Stačí zajet na Lužiny!','2022-03-09 10:17:31.063','2022-03-09 10:16:03.575','http://www.hristepraha.cz/hriste/mapa/centralni-park-prahy-13','http://www.hristepraha.cz/images/img/9bf858de61f4e14add5c39a944b4b7fdo.jpg','Praha 13','Stodůlky','Česko','Pod Hranicí, 155 00 Hlavní město Praha-Stodůlky, Česko','Hlavní město Praha','155 00','Pod Hranicí'),
	 (4,'SRID=4326;POINT (14.572277069 50.105449677)'::geometry,'V zeleni u panelových domů, mezi ulicemi Breitcetlova a Šebelova (u mateřské školky), se rozkládá první areál (72.A)...',81,'Černý Most - hřiště 72.A','Na sídlišti Černý Most můžete nedaleko sebe navštívit dva menší areály s dětskými hřišti.','2022-03-09 10:17:31.063','2022-03-09 10:16:03.575','http://www.hristepraha.cz/hriste/mapa/cerny-most-hriste-72-a','http://www.hristepraha.cz/images/img/05884db0382d642111108750be85da6fo.jpg','Praha 14','Černý Most','Česko','Šebelova 875/4, 19800 Hlavní město Praha-Černý Most, Česko','Hlavní město Praha','19800','Šebelova 875/4');

INSERT INTO playgrounds.playground_properties (playground_id,properties_id,updated_at,created_at) VALUES
	 (1,4,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782'),
	 (1,9,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782'),
	 (2,9,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782'),
	 (3,5,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782'),
	 (3,6,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782'),
	 (3,8,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782'),
	 (4,5,'2022-03-09 10:17:31.782','2022-03-09 10:16:05.782');
